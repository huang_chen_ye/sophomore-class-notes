﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public static class Control
    {
        public static void PrintInfo(this List<int> ls)
        {
            foreach (var item in ls)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }
        public static void PrintStudent(this List<Student> students)
        {
            Console.WriteLine(string.Format("Id\t\t名称\t性别\t身高\t"));
            foreach (var item in students)
            {
                Console.WriteLine(string.Format("{0}\t{1}\t{2}\t{3}\t",item.Id,item.Name,item.Sex,item.Height));
            }
            Console.WriteLine();
        }
       public static List<int> Query( this List<int> ls2, int x)
        {
            List<int> newList = new List<int>();
            foreach (var item in ls2)
            {
                if (item > x)
                {
                    newList.Add(item);
                }

            }
            return newList;

        }
       public static List<Student> Query2(this List<Student> ls3,Func<Student,bool> func)
        {
            List<Student> students = new List<Student>();
            foreach (var item in ls3)
            {
                if (func(item))
                {
                    students.Add(item);
                }
            }
            return students;

        }
      
    }
}
