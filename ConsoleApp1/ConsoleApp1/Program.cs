﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            ///生成50的数字 并调用printINfo这个方法输出数字
            /*List<int> number = new List<int>(); 
            for(int i=0;i<50;i++)
                {
              
                number.Add(i) ;
            
                }
            PrintInfo(number);
            Console.ReadLine();
            */
            ///随机生成50个数字，并运用printInfo这个方法打印出来
           List<int> number = new List<int>();
            List<Student> people = new List<Student>();
            Random random = new Random();
            for (int i = 0; i < 50; i++)
            {
                var temp = new Student
                {
                    Id = random.Next(),
                    Name = "玖儿" + random.Next(1, 100),
                    Sex = random.Next(0, 2) > 0 ? "男" : "女",
                    Height = random.Next(150, 200)

                };
                people.Add(temp);
            }
            people.PrintStudent();
            people.Query2(x=>x.Height>190 && x.Sex=="女").PrintStudent();

            number.PrintInfo();
            Console.WriteLine();
            number.Query(180).PrintInfo();
            
            Console.ReadLine();
           
          

        }


     
    }
}
