﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
   public class FibonacciClass
    {  
        //f(1)=1,f(2)=2,f(n)=f(n-1)+(n-2)(n>=3)
        //法一：
        public static int Fibonacci(int n)
        {
            int result;
            if (n == 1 || n == 2)
            {
                return 1;
            }
          
            result = Fibonacci(n - 1) + Fibonacci(n - 2);
            return result;
        }
        //法二：
        public static int Fibonacci2(int x)
        { 
            
            if (x == 1 || x == 2)
            {
                return 1;
            }
            else 
            {
                int first = 1;//n-1项
                int last = 1;//n-2项
                int result2 = 0;
                for(int i = 3;i <= x;i++)
                    {
                    result2 = first + last;
                    first = last;
                    last = result2;

                    }
                return result2;
            }
        
        }
    }
}
